-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2018 at 02:55 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cashier`
--

CREATE TABLE `cashier` (
  `id` int(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `orda_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chef`
--

CREATE TABLE `chef` (
  `email` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `sid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chef`
--

INSERT INTO `chef` (`email`, `password`, `sid`) VALUES
('dephinek@gmail.com', '', 1),
('dephinek@gmail.com', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `phone_no` int(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `comment` text NOT NULL,
  `com_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `fname` varchar(11) NOT NULL,
  `lname` varchar(11) NOT NULL,
  `city` varchar(11) NOT NULL,
  `street` varchar(11) NOT NULL,
  `contact` int(11) NOT NULL,
  `house_no` varchar(11) NOT NULL,
  `plate_no` int(11) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`fname`, `lname`, `city`, `street`, `contact`, `house_no`, `plate_no`, `password`) VALUES
('hgvghhg', 'hjh', 'nnm', 'nn', 78, 'nh', 1, ''),
('trtrtr', 'thfhg', 'hgghfhggh', 'fhfhhg', 234, '45', 3, ''),
('trtrtr', 'thfhg', 'hgghfhggh', 'fhfhhg', 236, '45', 3, ''),
('tyty', 'trtrtr', 'trtrtrt', 'trtrtr', 546, '6', 0, ''),
('rttrkjkh', 'ghfhgfg', 'hgfhgghfg', 'ghdghfh', 4355, '434', 0, ''),
('tyty656', 'trtrtr', 'trtrtrt', 'trtrtr', 123344, '6', 0, ''),
('tyty656', 'trtrtr', 'trtrtrt', 'trtrtr', 54665757, '6', 0, ''),
('mahir', 'juma', 'morogoro', 'lukobe', 75643678, '43', 2, ''),
('prisca', 'kelvin', 'morogoro', 'mazimbu', 654350902, '431', 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `orda_for_cooking`
--

CREATE TABLE `orda_for_cooking` (
  `oid` int(20) NOT NULL,
  `id` int(10) NOT NULL,
  `sid` int(10) NOT NULL,
  `orda_id` varchar(20) NOT NULL,
  `cashier_coment` text,
  `chef_coment` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `orda_id` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `food_name` varchar(30) NOT NULL,
  `contact` int(11) NOT NULL,
  `orda_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cashier`
--
ALTER TABLE `cashier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orda_id` (`orda_id`);

--
-- Indexes for table `chef`
--
ALTER TABLE `chef`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `id` (`sid`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`contact`);

--
-- Indexes for table `orda_for_cooking`
--
ALTER TABLE `orda_for_cooking`
  ADD PRIMARY KEY (`oid`),
  ADD KEY `orda_id` (`orda_id`),
  ADD KEY `sid` (`sid`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`orda_id`),
  ADD KEY `contact` (`contact`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cashier`
--
ALTER TABLE `cashier`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `chef`
--
ALTER TABLE `chef`
  MODIFY `sid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `com_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orda_for_cooking`
--
ALTER TABLE `orda_for_cooking`
  MODIFY `oid` int(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cashier`
--
ALTER TABLE `cashier`
  ADD CONSTRAINT `cashier_ibfk_1` FOREIGN KEY (`orda_id`) REFERENCES `order` (`orda_id`);

--
-- Constraints for table `orda_for_cooking`
--
ALTER TABLE `orda_for_cooking`
  ADD CONSTRAINT `orda_for_cooking_ibfk_1` FOREIGN KEY (`orda_id`) REFERENCES `order` (`orda_id`),
  ADD CONSTRAINT `orda_for_cooking_ibfk_2` FOREIGN KEY (`sid`) REFERENCES `chef` (`sid`),
  ADD CONSTRAINT `orda_for_cooking_ibfk_3` FOREIGN KEY (`id`) REFERENCES `cashier` (`id`);

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`contact`) REFERENCES `customer` (`contact`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
