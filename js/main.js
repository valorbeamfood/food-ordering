var options = {
  strings: [" Lorem ipsum dolor sit amet, consectetur adipisicing elit. A molestiae impedit vitae saepe vel incidunt. A sed deleniti, possimus similique"],
  typeSpeed: 40,
  smartBackspace: false,
  loop: true
}

particlesJS.load('particles-js', 'js/vendors/particles.json', function() {
  console.log('callback - particles.js config loaded');
});

var typed = new Typed(".tagline", options);