<!DOCTYPE html>
<html lang="en">
<?PHP session_start(); 

	if(!isset($_SESSION['error'])){
		$_SESSION['error'] = "";
	}


?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Food Ordering Management System</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">
	<style>
		.enternumber{
			width:150px;
			border-radius:10px;
			border: 1px solid black;
			height:30px;
			font-size:18px;
			padding-left:4px; 
		}
	
	</style>
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-primary navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a class="navbar-brand" href="#">
                    <img src="img/overlays/logo.png">
                </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Home
                                <span class="sr-only">(current)</span>
                            </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#product">Menu</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#index,html">Contact Us</a>
                    </li>
                    <li class="nav-item btn-group">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>
                            </a>
                        <div class="dropdown-menu dropdown-white" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="chef-login.form.php">Chef</a>
                            <a class="dropdown-item" href="casherlogin.php">Cashier</a>

                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </nav>


    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel" style= "margin-top:-200px;">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-2" data-slide-to="1"></li>
            <li data-target="#carousel-example-2" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <div class="view">
                    <img class="d-block w-100" src="img/overlays/slider1.jpg" alt="First slide">
                    <div class="mask rgba-black-slight"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">vegetable salads</h3>
                    <p>Ina virutubisho vyote hasa kwa wanaopunguza uzito</p>
                </div>
            </div>
            <div class="carousel-item">
                <!--Mask color-->
                <div class="view">
                    <img class="d-block w-100" src="img/overlays/slider2.jpg" alt="Second slide">
                    <div class="mask rgba-black-slight"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">tambi vegetable</h3>
                    <p>Yummy and tasty..</p>
                </div>
            </div>
            <div class="carousel-item">
                <!--Mask color-->
                <div class="view">
                    <img class="d-block w-100" src="img/overlays/slider3.jpg" alt="Third slide">
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">Lemon kuku</h3>
                    <p>Full spicy(wenye viungo vyote)</p>
                </div>
            </div>
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->


        <section class="about md-5 pd-5 ">
        <div class="container">
            <h1 class="text-center mt-5 mb-5 pt-5 pb-3 animated bounce">
                <em>Meet Our Team</em>
            </h1>

            <div class="row">

                <div class="col-xl-5 mr-auto mb-r col-lg-6">
                    <img src="img/overlays/team.jpg" class="img-fluid rounded z-depth-1-half" alt="My photo">
                </div>

                <div class="col-xl-6 col-lg-6 pb-3 wow fadeIn" data-wow-delay="0.4s">

                    <!-- Description -->
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo animi soluta ratione quisquam, dicta ab cupiditate iure eaque? Repellendus voluptatum, magni impedit eaque animi maxime.</p>

                    <p align="justify">Nemo animi soluta ratione quisquam, dicta ab cupiditate iure eaque? Repellendus voluptatum, magni impedit eaque delectus, beatae maxime temporibus maiores quibusdam quasi rem magnam.</p>

                    <ul class="text-muted">
                        <li>Nemo animi soluta ratione</li>
                        <li>Beatae maxime temporibus</li>
                        <li>Consectetur adipisicing elit</li>
                    </ul>

                </div>
            </div>
        </div>
    </section>

    
<!--Section menu-->

    <section class="section" id="specials">
        <div class="container">
            
            <!-- Secion heading -->
            <h1 class="text-center font-bold mt-5 pt-4 mb-5 pb-3 drk-grey-text wow fadeIn" data-wow-delay="0.2s">
                <em>Our Special Dishes</em>
            </h1>

            <p class="grey-text text-center ml-3 mr-3 mt-1 mb-5">
				wellcome to Food ordering website, from this site you can choose the kind of food you want to order
				<br>
				<?PHP echo $_SESSION['error'];  unset($_SESSION['error']); ?>
            </p>

            <div class="row text-center mr-2 ml-2 mt-3">
                <div class="col-lg-4 col-md-12 mb-r">

                    <div class="view overlay">
                        <img src="img/overlays/p1.png" class="img-fluid" alt="">
                    <div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text text-center">Eggs noodles</h3>
                        	<p class="white-text">7500Tsh</p> 
                        </div>
                    </div>
                    <span style="font-family:tahoma; font-size:22;"> food number : 1</span>
                    <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order now</a>
                    </div>
                </div>

									<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">

										<div class="view overlay">
											<img src="img/overlays/p4.png" class="img-fluid" alt="">
											<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">Rolled samosas</h3>
                        	<p class="white-text">4000Tsh</p> 
                                            </div>
										</div>
                                        <span style="font-family:tahoma; font-size:22;"> food number: 2</span>
										
										  <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header text-center">
									<h4 class="modal-title w-100 font-weight-bold text-success">Welcome </h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
					
				<form action="register_customer.php" method="post">
					<div class="modal-body">
										<!-- Extended material form grid -->
						<!-- Grid row -->
						<div class="form-row">
							<!-- Grid column -->
							<div class="col-md-6">
								<!-- Material input -->
								<div class="md-form form-group">
									<input type="text" class="form-control" name="fname" id="inputname4MD" placeholder="Name">
									<label for="inputname4MD">Name</label>
								</div>
							</div>
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-md-6">
								<!-- Material input -->
								<div class="md-form form-group">
									<input type="text" class="form-control" name="city" id="inputAddressMD" Value="Morogoro" placeholder="Morogoro">
									<label for="inputAddressMD">City</label>
								</div>
							</div>
							<!-- Grid column -->
						</div>
						<!-- Grid row -->

						<!-- Grid row -->
						<div class="row">
							
							<!-- Grid column -->

							<!-- Grid column -->
							<div class="col-md-6">
								<!-- Material input -->
								<div class="md-form form-group">
									 <input type="text" class="form-control "name="street" id="inputAddress2MD" placeholder="eg. mazimbu road">
									 <label for="inputAddress2MD">Street</label>
								</div>
							</div>
							<!-- Grid column -->
							<div class="col-md-6">
								<!-- Material input -->
								<div class="md-form form-group">
									<input type="text" class="form-control" name="contact" id="inputCityMD" placeholder="+255">
									<label for="inputCityMD">Phone</label>
								</div>
							</div>
							
						</div>
						<!-- Grid row -->

						  
						  <h4 class="text-success">Enter food number</h4>
						<div class="form-check mb-4">
							<input type="text" class="enternumber" name="food" id="inputZipMD" placeholder="food number" >
						</div> 
						
						<h4 class="text-success">Select number of plates</h4>
						<div class="form-check mb-4">
							<input  type="text" class="enternumber"  name="plate" id="radioWithGap4" checked="checked" placeholder="example 3" >
							
						</div>
									
						  <!-- Grid row -->
						   
						<!--
						<div class="md-form">
							<input placeholder="Selected time" type="text" id="input_starttime" class="form-control timepicker">
							<label for="input_starttime">Light version, 12hours</label>
						</div>
						-->


						<!-- Extended material form grid -->
						</div>
								
								<div class="modal-footer d-flex justify-content-center">
									<button type="submit" name="submit" class="btn btn-teal">ORDER</button>
								</div>
							</div>
						</div>
					</div>
			
             </form>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">order now</a>
					</div>
				</div>

				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">

					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0018.jpg" class="img-fluid" alt="">
                        <div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">beaf burger</h3>
                        	<p class="white-text">16000Tsh</p> 
                        </div>
					</div>
						<span style="font-family:tahoma; font-size:22;"> food number : 3</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--Card-->

				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">

					<div class="view overlay">
						<img src="img/overlays/p6.png" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">roasted cassava with spicy vegetables</h3>
                        	<p class="white-text">7000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number :4</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
									
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">

					<div class="view overlay">
						<img src="img/overlays/p2.png" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">Italian Meat</h3>
                        	<p class="white-text">8000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 5</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
									
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0020.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">SOUSAGE ROLLS</h3>
                        	<p class="white-text">5000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 6</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				
                  <div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">

					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0016.jpg"  class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">ugali na samaki wa nazi</h3>
                        	<p class="white-text">3500Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 7</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0017.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">boilled vegeterian macarony</h3>
                        	<p class="white-text">1000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 8</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0019.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">pancake served with soup</h3>
                        	<p class="white-text">6000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 9</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0021.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">wali njegere</h3>
                        	<p class="white-text">5000Tsh</p> 
                        </div>
					</div>
                    <span style="font-family:tahoma; font-size:22;"> food number : 10</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0022.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">tambi na samaki kamba</h3>
                        	<p class="white-text">10000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 11</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0025.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text ">CARTLES ZA KUKU</h3>
                        	<p class="white-text">7000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 12</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0024.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">Tambi sousage</h3>
                        	<p class="white-text">8000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 13</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				<div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/IMG-20180515-WA0023.jpg" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">ugali maini na kuku</h3>
                        	<p class="white-text">9000Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 14</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
                    
                    <div class="col-lg-4 col-md-12 mb-r wow fadeIn" data-wow-delay="0.4s">
					<div class="view overlay">
						<img src="img/overlays/p5.png" class="img-fluid" alt="">
						<div class="mask rgba-black-strong text-caption">
                           <h3 class="white-text">mkate na salad</h3>
                        	<p class="white-text">1500Tsh</p> 
                        </div>
					</div>
					<span style="font-family:tahoma; font-size:22;"> food number : 15</span>
					<div class="text-center">
						<a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Order Now</a>
					</div>
				</div>
				<!--/.Card-->
				
				
				
				
				
            </div>


        </div>


    </section>



<!-- Section: Testimonials -->
<div class="container">

<!-- Section: Testimonials v.3 -->
<section class="team-section text-center my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold my-5">Testimonials v.3</h2>
  <!-- Section description -->
  <p class="dark-grey-text w-responsive mx-auto mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam eum porro a pariatur veniam.</p>

  <!--Grid row-->
  <div class="row text-center">

    <!--Grid column-->
    <div class="col-md-4 mb-md-0 mb-5">

      <div class="testimonial">
        <!--Avatar-->
        <div class="avatar mx-auto">
          <img src="img/overlays/woza.png" class="rounded-circle z-depth-1 img-fluid">
        </div>
        <!--Content-->
        <h4 class="font-weight-bold dark-grey-text mt-4">John Doe</h4>
        <h6 class="font-weight-bold blue-text my-3">Pastry Chef</h6>
        <p class="font-weight-normal cyan-text">
          <i class="fa fa-quote-left pr-2 lime-text"></i>if you want to become a great chef, you have to work with great chefs. And that's exactly what I did.</p>
        <!--Review-->
        <div class="orange-text">
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star-half-full"> </i>
        </div>
      </div>

    </div>
    <!--Grid column-->

    <!--Grid column-->
    <div class="col-md-4 mb-md-0 mb-5">

      <div class="testimonial">
        <!--Avatar-->
        <div class="avatar mx-auto">
          <img src="img/overlays/chif0.1.png" class="rounded-circle z-depth-1 img-fluid">
        </div>
        <!--Content-->
        <h4 class="font-weight-bold dark-grey-text mt-4">Anna Deynah</h4>
        <h6 class="font-weight-bold blue-text my-3">Sous Chef</h6>
        <p class="font-weight-normal cyan-text">
          <i class="fa fa-quote-left pr-2 lime-text"></i>Techniques are not the most difficult to teach. The attitudes chefs take are much more important.</p>
        <!--Review-->
        <div class="orange-text">
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
        </div>
      </div>

    </div>
    <!--Grid column-->

    <!--Grid column-->
    <div class="col-md-4">

      <div class="testimonial">
        <!--Avatar-->
        <div class="avatar mx-auto">
          <img src="img/overlays/chef3.png" class="rounded-circle z-depth-1 img-fluid">
        </div>
        <!--Content-->
        <h4 class="font-weight-bold dark-grey-text mt-4">Brad Ford</h4>
        <h6 class="font-weight-bold blue-text my-3">Commis Chef</h6>
        <p class="font-weight-normal cyan-text">
          <i class="fa fa-quote-left pr-2 lime-text"></i>You have one life to live; do your work with passion and give your best. always be best and keep cooking</p>
        <!--Review-->
        <div class="orange-text">
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star"> </i>
          <i class="fa fa-star-o"> </i>
        </div>
      </div>

    </div>
    <!--Grid column-->

  </div>
  <!--Grid row-->

</section>
<!-- Section: Testimonials v.3 -->
            
    </div>
<!-- Section: Testimonials  -->

<footer class="page-footer font-small unique-color-dark pt-0">





    <!--Copyright-->
     <div class="container text-center">
            <div class="row">
                <div class="col">
                    <div class="copyright">
                        <p>Powered By Prisca Johnson &copy; 2018. All Right Reseverd</p>
                    </div>
                </div>
            </div>
        </div>
    <!--/Copyright-->

</footer>








    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>


</body>

</html>
