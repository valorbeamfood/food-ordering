<!doctype html>
<html>
<head>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>food ordering</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">
</head>

</head>

<body>
<div class="header">
<div style="height: 100vh">
	  <div class="flex-center flex-column">
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Welcome Chief</h4> <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="table-warning">
                                    <th>order iD</th>
                                    <th>Food Name</th>
                                    <th>Quantity</th>
                                    <th>phone number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>chinese fish catlage</td>
                                    <td>1 plate</td>
                                    <td>0652636640</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>Caesar salad</td>
                                    <td>1 plate</td>
                                    <td>0784510738</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>Beef steak</td>
                                    <td>2 plates</td>
                                    <td>0712345647</td>
                                </tr>
                                <tr>
                                    <th scope="row">4</th>
                                    <td>Dumplings</td>
                                    <td>6 pieces</td>
                                    <td>0623490958</td>
                                </tr>
                                <tr>
                                    <th scope="row">5</th>
                                    <td>Apple cake</td>
                                    <td>1 piece</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                


<!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
</body>
</html>