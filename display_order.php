 <!DOCTYPE html>
<html lang="en">

<head>
    <style>
        .cook{
            width:90px;
            height:35px;
            border-radius: 20px;
            background-color: green;
            color:white;
        }
    </style>
    
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Food Ordering System</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <!--  Project start  -->

    <!--Navbar-->

    <nav class="navbar navbar-expand-sm bg-primary navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <a class="navbar-brand" href="#">
                    <strong>Food Ordering</strong>
                </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Home
<!--                                <span class="sr-only">(current)</span>-->
                            </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="menu.php">Menu</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#Contact">Contact Us</a>
                    </li>
                    <li class="nav-item btn-group">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>
                            </a>
                        <div class="dropdown-menu dropdown-white" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="chef-login.form.php">Chef</a>
                            <a class="dropdown-item" href="casherlogin.php">Cashier</a>

                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
     <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel" style= "margin-top:-200px;">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-2" data-slide-to="1"></li>
            <li data-target="#carousel-example-2" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <div class="view">
                    <img class="d-block w-100" src="img/overlays/pexels-phoato-572056.jpeg" alt="First slide">
                    <div class="mask rgba-black-slight"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">Everything starts with technology</h3>
                    <p>Aiming at providing the best</p>
                </div>
            </div>
            <div class="carousel-item">
                <!--Mask color-->
                <div class="view">
                    <img class="d-block w-100" src="img/overlays/BUSINESS.jpeg" alt="Second slide">
                    <div class="mask rgba-black-slight"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">Business is filled with stratergies</h3>
                    <p>Serving people is a number 1 priority</p>
                </div>
            </div>
            <div class="carousel-item">
                <!--Mask color-->
                <div class="view">
                    <img class="d-block w-100" src="img/overlays/IPAD.jpeg" alt="Third slide">
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">Unity solidifies work.</h3>
                    <p>The wishes of our customers is our command</p>
                </div>
            </div>
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
    
    
   <div class="container">
     <div class="table-responsive my-4 pb-5 ">
        <table class="table table-bordered">
            <thead>
                <tr class="table-success">
                    <th>No</th>
                    <th>Name</th>
                    <th>city</th>
                    <th>street</th>
                    <th>contact</th>
                    <th>food name</th>
                    <th>Plate</th>
                    <th>cost</th>
                    <th>send to cheif</th>
                </tr>
            </thead>
            <tbody>
<?php
    $host="localhost";
    $user="root";
    $password="";
    $db="final2";
//connect to database
    $conn = mysqli_connect($host,$user,$password);
    if(!$conn){
        echo "fail to connect to database";
    }
//test connection 
    //select the database
    $select_db = mysqli_select_db($conn , $db);
    if(!$select_db)
        echo "fail to select database "; 
    else
    //echo "successfully select database ";
     
      
    $sql = "SELECT * FROM `custom_order` WHERE 1 ; ";
    $run_oder = mysqli_query($conn, $sql);
                         
    while($data =  mysqli_fetch_array($run_oder)){
        $orda_id = $data['orda_id']; 
        $food_no = $data['food_no'];
        $orda_comment = $data['orda_comment'];
        $plate_no = $data[ 'plate_no'];
        $cost_id = $data['cost_id'];
        
            // fetch customer from customer table
            $sql_cost = "SELECT * FROM customer WHERE cost_id = '{$cost_id}'; ";
            $run_cost = mysqli_query($conn , $sql_cost);
            while($record =  mysqli_fetch_array($run_cost)){
                $name = $record['name'];
                $city = $record['city'];
                $street = $record['street'];
                $contact = $record['contact'];
            }
            // identify food by food number above
             $sql_food = "SELECT * FROM food WHERE food_no = '{$food_no}'; ";
            $run_food = mysqli_query($conn , $sql_food);
            while($record1 =  mysqli_fetch_array($run_food)){
                $food_name = $record1['food_name'];
                $price = $record1['price'];
            }
        $cost = $plate_no * $price;
        echo "
            <tr>
                <td>{$orda_id}</td>
                <td>{$name}</td>
                <td>{$city}</td>
                <td>{$street}</td>
                <td>{$contact}</td>
                <td>{$food_name}</td>
                <td>{$plate_no}</td>
                <td>{$cost}</td>
                <td>
                    <button class='cook' onclick=''> send </button>
                </td>
            </tr>
            "; 
    }
                
    ?>
         </tbody>
        </table>
    </div>
    </div>